<%@page import="java.util.List"%>
<%@page import="model.Book"%>
<%
    if (request.getAttribute("books") != null) {
        List<Book> books = (List<Book>) request.getAttribute("books");
        StringBuilder dynamicTable = new StringBuilder("");
        if (session.getAttribute("username") == null) {
            dynamicTable.append("<table><thead><tr>"
                    + "<th scope='col'>Id</th>"
                    + "<th scope='col'>tipusPub</th>"
                    + "<th scope='col'>categoria</th>"
                    + "<th scope='col'>titol</th>"
                     + "<th scope='col'>descripcio</th>"
                    + "<th scope='col'>editorial</th>"
                    + "</tr></thead><tbody>");
            for (Book book : books) {
                dynamicTable.append(String.format("<tr>"
                        + "<td scope='row'>%d</td>"
                        + "<td>%s</td>"
                        + "<td>%s</td>"
                        + "<td>%s</td>"
                        + "<td>%s</td>"
                        + "<td>%s</td>"
                        + "</tr>",
                        book.getId(),
                        book.getTipusPub(),
                        book.getCategoria(),
                        book. getTitol(),
                        book.getDescripcio(),
                        book. getEditorial()));
            }
        } else {
            dynamicTable.append("<table><thead><tr>"
                    + "<th scope='col'>Id</th>"
                    + "<th scope='col'>tipusPub</th>"
                    + "<th scope='col'>categoria</th>"
                    + "<th scope='col'>titol</th>"
                    + "<th scope='col'>descripcio</th>"
                    + "<th scope='col'>editorial</th>"
                    + "</tr></thead><tbody>");
            for (Book book : books) {
                dynamicTable.append(String.format("<tr>"
                        + "<td scope='row'>%d</td>"
                        + "<td>%s</td>"
                        + "<td>%s</td>"
                        + "<td>%s</td>"
                        + "<td>%s</td>"
                        + "<td>%s</td>"
                        + "</tr>",
                        book.getId(),
                        book.getTipusPub(),
                        book.getCategoria(),
                        book. getTitol(),
                        book.getDescripcio(),
                        book. getEditorial()));
            }
        }
        out.println(dynamicTable.toString());
    }
%>