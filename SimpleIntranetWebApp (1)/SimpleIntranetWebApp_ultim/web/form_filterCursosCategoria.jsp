<% if (request.getAttribute("form_filterCursosCategoria") != null) { %>

<nav class="navbar navbar-nav bg-light align-right">
    <form class="form-inline align-right" id="filter_form" method="post" action="book">
        <div class="input-group mb-2 mr-sm-2">
            <div class="input-group-prepend">
                <div class="input-group-text">Categoria</div>
            </div>
            <select class="form-control custom-select mr-sm-2" name="categoria" id="categoria_filter">
                <option value="llibres">llibres</option>
                <option value="noticies">noticies</option>
                <option value="webs">webs</option>
            </select>
            
        </div>
        <button type="submit" form="filter_form" class="btn btn-primary mb-2" name="action" value="filterCategoria">Filter per categoria</button>
    </form>
</nav>

<%}%>