package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Alejandro Asensio
 * @version 1.0, 2019-01-29
 */
public class BookDAO {

    private DataBase d; // object of class DataBase

    public BookDAO() {
    }

    public BookDAO(String path) {
        d = new DataBase(path + "/files/books.csv");
    }

    /**
     * Check if a given patient is registered in the osteoporosis file.
     *
     * @param patient Patient to be found
     * @return boolean true if patient's registerId is in file; false otherwise
     */
    public boolean find(Patient patient) {
        boolean found = false;
        List<String> all = d.listAllLines();
        for (String s : all) {
            String[] pieces = s.split(";");
            if (pieces[0].equals(patient.getRegisterId())) {
                found = true;
                break;
            }
        }
        return found;
    }

    /**
     * Lists all books in database.
     *
     * @return a List of Book objects
     */
    public List listAll() {
        List<Book> books = new ArrayList<>();
        List<String> all = d.listAllLines();

        for (String s : all) {
            String[] pieces = s.split(";");
            Book p = new Book();

            int id = Integer.parseInt(pieces[0]);
            String tipusPub = pieces[1];
            String categoria = pieces[2];
            String titol = pieces[3];
            String descripcio = pieces[4];
            String editorial = pieces[5];

            books.add(new Book(id, tipusPub, categoria, titol, descripcio, editorial));
        }

        return books;
    }

    /**
     * Lists the patients filtered by a patient object.
     *
     * @param filterPatient Patient object
     * @return a List of Patient objects
     */
    public List listFilteredType(String type) {
        List<Book> allCurs = listAll();
        List<Book> filteredCurs = new ArrayList<>();

        if (type.equals("ONLINE") || type.equals("PRESENCIAL")) {
            for (Book p : allCurs) {
                boolean match
                        = p.getTipusPub().equals(type);

                if (match) {
                    filteredCurs.add(p);
                }
            }
        }

        return filteredCurs;
    }

    public List listFiltered(Book filterBook) {
        List<Book> allBooks = listAll();
        List<Book> filteredBooks = new ArrayList<>();
        
        String CATEGORIA = filterBook.getCategoria();
               // .toUpperCase();
       
        for (Book p : allBooks) {
            boolean match = 
               p.getCategoria().equals(CATEGORIA);
                    
            if (match) {
                filteredBooks.add(p);
            }
        }
        return filteredBooks;

    }

}
