package model;

/**
 * Models a book register.
 *
 * @author Alejandro Asensio
 * @version 1.0, 2019-01-29
 */
public class Book {

   private int id;
   private String tipusPub; //digital o impesa.
   private String categoria; // llibres, notícies o webs
   private String titol;
   private String descripcio;
   private String editorial;

    public Book(int id, String tipusPub, String categoria, String titol, String descripcio, String editorial) {
        this.id = id;
        this.tipusPub = tipusPub;
        this.categoria = categoria;
        this.titol = titol;
        this.descripcio = descripcio;
        this.editorial = editorial;
    }

    public Book() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipusPub() {
        return tipusPub;
    }

    public void setTipusPub(String tipusPub) {
        this.tipusPub = tipusPub;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getTitol() {
        return titol;
    }

    public void setTitol(String titol) {
        this.titol = titol;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Book{id=").append(id);
        sb.append(", tipusPub=").append(tipusPub);
        sb.append(", categoria=").append(categoria);
        sb.append(", titol=").append(titol);
        sb.append(", descripcio=").append(descripcio);
        sb.append(", editorial=").append(editorial);
        sb.append('}');
        return sb.toString();
    }
    


}
