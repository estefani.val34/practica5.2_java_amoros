package controllers;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Book;
import model.BookDAO;

/**
 * BookController
 *
 * @author
 * @version 2019-02-07
 */
@WebServlet(name = "BookController", urlPatterns = {"/book"})
public class BookController extends HttpServlet {

    private String path;
    private BookDAO pdao;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // calcula el ruta absoluta para llegar a WEB-INF 
        // Cuando hacemos Clean & Build, se genera otra estructura de directorios: LoginApplication/build/web/WEB-INF/
        path = getServletContext().getRealPath("/WEB-INF");
        pdao = new BookDAO(path);

        if (request.getParameter("action") != null) {
            String action = request.getParameter("action");
            switch (action) {
                case "load":
                    load(request, response);
                    break;
                case "form_filterCursosCategoria":
                    form_filterCursosCategoria(request, response);
                    break;
                case "filterCategoria":
                    filterCategoria(request, response);
                    break;
                case "filterCursosOnline":
                    filterCursosOnline(request, response);
                    break;
                case "filterCursosPresencial":
                    filterCursosPresencial(request, response);
                    break;
                default:
                    throw new AssertionError();
            }
        } else {
            response.sendRedirect("index.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Loads the books data.
     *
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    private void load(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Call the DAO, which calls the DataBase to get the data from the file
        List<Book> books = pdao.listAll();
        request.setAttribute("books", books);
        request.setAttribute("users", null);
        RequestDispatcher rd = request.getRequestDispatcher("landing_1.jsp");
        rd.forward(request, response);
    }

    /**
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    private void filterCursosOnline(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Book> filteredCurs = pdao.listFilteredType("ONLINE");
        request.setAttribute("books", filteredCurs);
        System.out.println("filterCursosOnline");
        RequestDispatcher rd = request.getRequestDispatcher("landing_1.jsp");
        rd.forward(request, response);

    }
    /**
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    private void filterCursosPresencial(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       
        List<Book> filteredCurs = pdao.listFilteredType("PRESENCIAL");
        request.setAttribute("books", filteredCurs); // cookie 

        System.out.println("filterCursosPresencial");
        RequestDispatcher rd = request.getRequestDispatcher("landing_1.jsp"); // manda aqui la informacion del cookie 
        rd.forward(request, response);

    }
    /**
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    private void form_filterCursosCategoria(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("form_filterCursosCategoria", "yes");
        RequestDispatcher rd = request.getRequestDispatcher("landing_1.jsp");
        rd.forward(request, response);
    }
    /**
     * 
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    private void filterCategoria(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String categoria = request.getParameter("categoria");

        Book p = new Book();
        p.setCategoria(categoria);

        List<Book> filteredBooks = pdao.listFiltered(p);
        request.setAttribute("books", filteredBooks);
        RequestDispatcher rd = request.getRequestDispatcher("landing_1.jsp");
        rd.forward(request, response);
    }

}
